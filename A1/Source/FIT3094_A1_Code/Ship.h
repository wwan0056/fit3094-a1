// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "GridNode.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Ship.generated.h"

class ALevelGenerator;



UCLASS()
class FIT3094_A1_CODE_API AShip : public AActor
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, Category = "Stats")
		float MoveSpeed;
	UPROPERTY(EditAnywhere, Category = "Stats")
		float Tolerance;

	

	

public:	
	// Sets default values for this actor's properties
	AShip();
	bool GeneratePath = true;
	TArray<GridNode*> Path;

	ALevelGenerator* level;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
