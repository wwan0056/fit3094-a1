// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelGenerator.h"

#include <algorithm>
#include "Engine/World.h"

// Sets default values
ALevelGenerator::ALevelGenerator()
{
	// Set this actor to call Tick() every frame. 
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ALevelGenerator::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ALevelGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (Ship && Ship->GeneratePath && GoldActors.Num() > 0)
	{
		CollectGold();
		ResetAllNodes();
		SpawnNextGold();
		//CalculateDFS();
		CalculateAStar();
		DetailPath();
	}
	// spawn gold
	if (GoldActors.Num() == 0)
	{
		SpawnNextGold();
	}
}

void ALevelGenerator::CollectGold()
{
	GoldArray.RemoveAt(0);
	GoldActors[0]->Destroy();
	GoldActors.RemoveAt(0);
}

void ALevelGenerator::GenerateWorldFromFile(TArray<FString> WorldArrayStrings)
{
	// If empty array exit immediately something is horribly wrong
	if (WorldArrayStrings.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("World Array is empty!"));
		return;
	}

	// Second line is Height (aka Y value)
	FString Height = WorldArrayStrings[1];
	Height.RemoveFromStart("Height ");
	MapSizeY = FCString::Atoi(*Height);
	UE_LOG(LogTemp, Warning, TEXT("Height: %d"), MapSizeY);

	// Third line is Width (aka X value)
	FString Width = WorldArrayStrings[2];
	Width.RemoveFromStart("width ");
	MapSizeX = FCString::Atoi(*Width);
	UE_LOG(LogTemp, Warning, TEXT("Width: %d"), MapSizeX);

	char CharMapArray[MAX_MAP_SIZE][MAX_MAP_SIZE];

	// Read through the Map section for create the CharMapArray
	for (int LineNum = 4; LineNum < MapSizeY + 4; LineNum++)
	{
		for (int CharNum = 0; CharNum < WorldArrayStrings[LineNum].Len(); CharNum++)
		{
			CharMapArray[LineNum - 4][CharNum] = WorldArrayStrings[LineNum][CharNum];
		}
	}

	// Read in the Gold positions
	for (int LineNum = 4 + MapSizeY; LineNum < WorldArrayStrings.Num(); LineNum++)
	{
		FString GoldX, GoldY;
		WorldArrayStrings[LineNum].Split(",", &GoldY, &GoldX);
		UE_LOG(LogTemp, Warning, TEXT("GoldX: %s"), *GoldX);
		UE_LOG(LogTemp, Warning, TEXT("GoldY: %s"), *GoldY);

		GoldArray.Add(FVector2D(FCString::Atof(*GoldX), FCString::Atof(*GoldY)));
	}

	GenerateNodeGrid(CharMapArray);
	SpawnWorldActors(CharMapArray);
	//CalculateDFS();
	CalculateAStar();
	DetailPath();
}

void ALevelGenerator::SpawnWorldActors(char Grid[MAX_MAP_SIZE][MAX_MAP_SIZE])
{
	UWorld* World = GetWorld();

	// Make sure that all blueprints are connected. If not then fail
	if (DeepBlueprint && ShallowBlueprint && LandBlueprint)
	{
		// For each grid space spawn an actor of the correct type in the game world
		for (int x = 0; x < MapSizeX; x++)
		{
			for (int y = 0; y < MapSizeY; y++)
			{
				float XPos = x * GRID_SIZE_WORLD;
				float YPos = y * GRID_SIZE_WORLD;

				FVector Position(XPos, YPos, 0);

				switch (Grid[x][y])
				{
				case '.':
					World->SpawnActor(DeepBlueprint, &Position, &FRotator::ZeroRotator);
					break;
				case '@':
					World->SpawnActor(LandBlueprint, &Position, &FRotator::ZeroRotator);
					break;
				case 'T':
					World->SpawnActor(ShallowBlueprint, &Position, &FRotator::ZeroRotator);
					break;
				default:
					break;
				}
			}
		}
	}

	// Generate Initial Agent Positions
	if (ShipBlueprint)
	{
		int XPos = 16; //Default Intial X Position
		int YPos = 23; //Default Intial Y Position

		FVector Position(XPos * GRID_SIZE_WORLD, YPos * GRID_SIZE_WORLD, 20);
		Ship = World->SpawnActor<AShip>(ShipBlueprint, Position, FRotator::ZeroRotator);
		Ship->level = this;

		WorldArray[XPos][YPos]->ObjectAtLocation = Ship;
		StartNode = WorldArray[XPos][YPos];
	}

	if (GoldBlueprint)
	{
		SpawnNextGold();
	}

	// Set Static Camera Position
	if (Camera)
	{
		FVector CameraPosition = Camera->GetActorLocation();

		CameraPosition.X = MapSizeX * 0.5 * GRID_SIZE_WORLD;
		CameraPosition.Y = MapSizeY * 0.5 * GRID_SIZE_WORLD;

		Camera->SetActorLocation(CameraPosition);
	}
}

void ALevelGenerator::SpawnNextGold()
{
	UWorld* World = GetWorld();
	// Generate next Gold Position
	if (GoldBlueprint && GoldArray.Num() > 0)
	{
		int XPos = GoldArray[0].X;
		int YPos = GoldArray[0].Y;


		FVector Position(XPos * GRID_SIZE_WORLD, YPos * GRID_SIZE_WORLD, 20);
		AGold* NewGold = World->SpawnActor<AGold>(GoldBlueprint, Position, FRotator::ZeroRotator);

		WorldArray[XPos][YPos]->ObjectAtLocation = NewGold;
		GoalNode = WorldArray[XPos][YPos];
		GoldActors.Add(NewGold);
	}
}

// Generates the grid of nodes used for pathfinding and also for placement of objects in the game world
void ALevelGenerator::GenerateNodeGrid(char Grid[MAX_MAP_SIZE][MAX_MAP_SIZE])
{
	for (int X = 0; X < MapSizeX; X++)
	{
		for (int Y = 0; Y < MapSizeY; Y++)
		{
			WorldArray[X][Y] = new GridNode();
			WorldArray[X][Y]->X = X;
			WorldArray[X][Y]->Y = Y;

			// Characters as defined from the map file
			switch (Grid[X][Y])
			{
			case '.':
				WorldArray[X][Y]->GridType = GridNode::DeepWater;
				break;
			case '@':
				WorldArray[X][Y]->GridType = GridNode::Land;
				break;
			case 'T':
				WorldArray[X][Y]->GridType = GridNode::ShallowWater;
				break;
			default:
				break;
			}
		}
	}
}

// Reset all node values (F, G, H & Parent)
void ALevelGenerator::ResetAllNodes()
{
	for (int X = 0; X < MapSizeX; X++)
	{
		for (int Y = 0; Y < MapSizeY; Y++)
		{
			WorldArray[X][Y]->F = 0;
			WorldArray[X][Y]->G = 0;
			WorldArray[X][Y]->H = 0;
			WorldArray[X][Y]->Parent = nullptr;
			WorldArray[X][Y]->IsChecked = false;
		}
	}
}

float ALevelGenerator::CalculateDistanceBetween(GridNode* first, GridNode* second)
{
	FVector distToTarget = FVector(second->X - first->X,
	                               second->Y - first->Y, 0);
	return distToTarget.Size();
}

void ALevelGenerator::DetailPath()
{
	//Onscreen Debug (Don't forget the include!)
	GEngine->AddOnScreenDebugMessage(-1, 12.f, FColor::White,
	                                 FString::Printf(
		                                 TEXT(
			                                 "Total Cells searched: %d with a path length of: %d and a distance of: %f"),
		                                 SearchCount, Ship->Path.Num(), CalculateDistanceBetween(StartNode, GoalNode)));
	GEngine->AddOnScreenDebugMessage(-1, 12.f, FColor::White,
	                                 FString::Printf(
		                                 TEXT(
			                                 "The difference between the current implemented path and the direct flight path is: %f"),
		                                 Ship->Path.Num() / CalculateDistanceBetween(StartNode, GoalNode)));
	//Log Debug message (Accessed through Window->Developer Tools->Output Log)
	UE_LOG(LogTemp, Warning, TEXT("Total Cells searched: %d with a path length of: %d and a distance of: %f"),
	       SearchCount, Ship->Path.Num(), CalculateDistanceBetween(StartNode, GoalNode));
	UE_LOG(LogTemp, Warning,
	       TEXT("The difference between the current implemented path and the direct flight path is: %f"),
	       Ship->Path.Num() / CalculateDistanceBetween(StartNode, GoalNode));
}


//-----------------------------------------------Edit the Code Below here!----------------------------------------------------------------


void ALevelGenerator::CalculateDFS()
{
	GridNode* currentNode = nullptr;
	GridNode* tempNode = nullptr;
	bool isGoalFound = false;

	TArray<GridNode*> nodesToVisit;

	StartNode->IsChecked = true;
	nodesToVisit.Add(StartNode);

	while (nodesToVisit.Num() >= 0)
	{
		SearchCount++;
		currentNode = nodesToVisit.Last();
		nodesToVisit.RemoveAt(nodesToVisit.Num() - 1);

		if (currentNode == GoalNode)
		{
			isGoalFound = true;
			break;
		}

		// Check the left neighbour
		// Check to ensure not out of range
		if (currentNode->Y - 1 >= 0)
		{
			// Get the Left neighbor from the list
			tempNode = WorldArray[currentNode->X][currentNode->Y - 1];
			// Check to make sure the node hasn't been visited AND is not closed (Land)
			if (tempNode->GridType != GridNode::Land && !tempNode->IsChecked)
			{
				tempNode->IsChecked = true;
				tempNode->Parent = currentNode;
				nodesToVisit.Add(tempNode);
			}
		}

		// Check the top neighbour
		// Check to ensure not out of range
		if (currentNode->X + 1 < MapSizeX - 1)
		{
			// Get the top neighbor from the list
			tempNode = WorldArray[currentNode->X + 1][currentNode->Y];
			// Check to make sure the node hasn't been visited AND is not closed (Land)
			if (tempNode->GridType != GridNode::Land && !tempNode->IsChecked)
			{
				tempNode->IsChecked = true;
				tempNode->Parent = currentNode;
				nodesToVisit.Add(tempNode);
			}
		}

		// Check the right neighbour
		// Check to ensure not out of range
		if (currentNode->Y + 1 < MapSizeY - 1)
		{
			// Get the right neighbor from the list
			tempNode = WorldArray[currentNode->X][currentNode->Y + 1];
			// Check to make sure the node hasn't been visited AND is not closed (Land)
			if (tempNode->GridType != GridNode::Land && !tempNode->IsChecked)
			{
				tempNode->IsChecked = true;
				tempNode->Parent = currentNode;
				nodesToVisit.Add(tempNode);
			}
		}

		// Check the buttom neighbour
		// Check to ensure not out of range
		if (currentNode->X - 1 > 0)
		{
			// Get the right neighbor from the list
			tempNode = WorldArray[currentNode->X - 1][currentNode->Y];
			// Check to make sure the node hasn't been visited AND is not closed (Land)
			if (tempNode->GridType != GridNode::Land && !tempNode->IsChecked)
			{
				tempNode->IsChecked = true;
				tempNode->Parent = currentNode;
				nodesToVisit.Add(tempNode);
			}
		}
	}

	if (isGoalFound)
	{
		RenderPath();
		Ship->GeneratePath = false;
	}
}

void ALevelGenerator::CalculateBFS()
{
	GridNode* currentNode = nullptr;
	GridNode* tempNode = nullptr;
	bool isGoalFound = false;

	TArray<GridNode*> nodesToVisit;
}

void ALevelGenerator::CalculateAStar()
{
	TArray<GridNode*> openList;
	TArray<GridNode*> closeList;

	GridNode* currentNode = nullptr;

	bool isGoalFound = false;
	//GridNode* nextNode = nullptr;

	// Clear list of open & close list 
	openList.Empty();
	closeList.Empty();

	StartNode->G = 0;
	StartNode->GetEstimateCost();
	StartNode->F = StartNode->G + StartNode->H;

	// Add start Node to OpenList
	openList.Add(StartNode);

	// While (OpenList is not empty)
	while (openList.Num() > 0)
	{
		// currentNode = RemoveNodeWithSmallest_f from OpenList
		currentNode = RemoveNodeWithSmallest_f(openList);
		if (currentNode)
		{
			//Add currentNode to ClosedList
			closeList.Add(currentNode);
			// If (currentNode is goalNode)
			if (currentNode == GoalNode)
			{
				UE_LOG(LogTemp, Warning, TEXT("Node has been found! [%d, %d]"), currentNode->X, currentNode->Y);
				isGoalFound = true;
				break;
			}

			for (auto& neighborNode : GetNeighbors(currentNode))
			{
				/*
				if (neighborNode->GridType !=GridNode::Land && !closeList.Contains(neighborNode))
				{
					// Set gcost
					int gCost = currentNode->GridType * (currentNode->G + CalculateDistanceBetween(currentNode, neighborNode));

					// 1. not contains neighbor in open list
					if (!openList.Contains(neighborNode))
					{
						// 2. overwrite current node gcost
						neighborNode->G=gCost;

						// 3. hcost
						neighborNode->GetEstimateCost();

						// 4. nextNode.cameFrom = currentNode
						neighborNode->Parent = currentNode;

						// 5. this neighbor node not contains in open list,
						//    then this neighbor node add to open list
						if (!openList.Contains(neighborNode))
						{
							openList.Add(neighborNode);
						}
					}
				}
				*/
				// If (nextNode is in ClosedList)
				if(neighborNode->GridType !=GridNode::Land && closeList.Contains(neighborNode))
				{
					//Skip nextNode
					continue;
				}
				else
				{
					//possible_g = currentNode.g + DistanceBetween(currentNode, nextNode);
					//G = cost factor * cost of movement; deep water is 1, shallow water is 2 here 
					int gCost = currentNode->GridType * currentNode->G +CalculateDistanceBetween(currentNode,neighborNode);
					//possible_g_isBetter = false
					bool gIsBetter = false;
					//If (nextNode is not in OpenList)
					if(!openList.Contains(neighborNode))
					{
						//Add nextNode to OpenList
						openList.Add(neighborNode);
						//nextNode.h = EstimateCost (nextNode, goalNode)
						neighborNode->GetEstimateCost();
						//possible_g_isBetter = true
						gIsBetter = true;
					}
					//Else If (possible_g < nextNode.g)
					else if (gCost<neighborNode->G)
					{
						//possible_g_isBetter = true
						gIsBetter =true;
					}

					//If (possible_g_isBetter is true)
					if(gIsBetter == true)
					{
						//nextNode.cameFrom = currentNode
						neighborNode->Parent = currentNode;
						//nextNode.g = possible_g
						neighborNode->G = gCost;
						//nextNode.f = nextNode.g + nextNode.h
						neighborNode->F = neighborNode->G + neighborNode->H;
					}
				}
			}
			/*
			// Check the left neighbour
			// Check to ensure not out of range
			if (currentNode->Y - 1 >= 0)
			{
				// Get the Left neighbor from the list
				nextNode = WorldArray[currentNode->X][currentNode->Y - 1];
				// Check to make sure the node hasn't been visited AND is not closed (Land)
				if (nextNode->GridType != GridNode::Land && closeList.Contains(nextNode))
				{
					// Skip nextNode
					continue;;
				}
				else
				{
					// possible_g = currentNode.g + DistanceBetween(currentNode, nextNode)
					float possible_g = currentNode->G + CalculateDistanceBetween(currentNode, nextNode);

					// possible_g_isBetter = false
					bool possible_g_isBetter = false;

					// If (nextNode is not in OpenList)
					if (!openList.Contains(nextNode))
					{
						// Add nextNode to OpenList
						openList.Add(nextNode);

						// nextNode.h = EstimateCost (nextNode, goalNode)
						nextNode->GetEstimateCost();

						// possible_g_isBetter = true
						possible_g_isBetter = true;
					}
					// Else If (possible_g < nextNode.g)
					else if (possible_g < nextNode->G)
					{
						// possible_g_isBetter = true
						possible_g_isBetter = true;
					}

					// If (possible_g_isBetter is true)
					if (possible_g_isBetter)
					{
						// nextNode.cameFrom = currentNode
						nextNode->Parent = currentNode;

						//nextNode.g = possible_g
						nextNode->G = possible_g;

						//nextNode.f = nextNode.g + nextNode.h
						nextNode->F = nextNode->G + nextNode->F;
					}
				}
			}

			// Check the top neighbour
			// Check to ensure not out of range
			if (currentNode->X + 1 < MapSizeX)
			{
				// Get the Left neighbor from the list
				nextNode = WorldArray[currentNode->X + 1][currentNode->Y];
				// Check to make sure the node hasn't been visited AND is not closed (Land)
				if (nextNode->GridType != GridNode::Land && closeList.Contains(nextNode))
				{
					// Skip nextNode
					continue;;
				}
				else
				{
					// possible_g = currentNode.g + DistanceBetween(currentNode, nextNode)
					float possible_g = currentNode->G + CalculateDistanceBetween(currentNode, nextNode);

					// possible_g_isBetter = false
					bool possible_g_isBetter = false;

					// If (nextNode is not in OpenList)
					if (!openList.Contains(nextNode))
					{
						// Add nextNode to OpenList
						openList.Add(nextNode);

						// nextNode.h = EstimateCost (nextNode, goalNode)
						nextNode->GetEstimateCost();

						// possible_g_isBetter = true
						possible_g_isBetter = true;
					}
					// Else If (possible_g < nextNode.g)
					else if (possible_g < nextNode->G)
					{
						// possible_g_isBetter = true
						possible_g_isBetter = true;
					}

					// If (possible_g_isBetter is true)
					if (possible_g_isBetter)
					{
						// nextNode.cameFrom = currentNode
						nextNode->Parent = currentNode;

						//nextNode.g = possible_g
						nextNode->G = possible_g;

						//nextNode.f = nextNode.g + nextNode.h
						nextNode->F = nextNode->G + nextNode->F;
					}
				}
			}

			// Check the right neighbour
			// Check to ensure not out of range
			if (currentNode->Y + 1 < MapSizeY - 1)
			{
				// Get the Left neighbor from the list
				nextNode = WorldArray[currentNode->X][currentNode->Y + 1];
				// Check to make sure the node hasn't been visited AND is not closed (Land)
				if (nextNode->GridType != GridNode::Land && closeList.Contains(nextNode))
				{
					// Skip nextNode
					continue;;
				}
				else
				{
					// possible_g = currentNode.g + DistanceBetween(currentNode, nextNode)
					float possible_g = currentNode->G + CalculateDistanceBetween(currentNode, nextNode);

					// possible_g_isBetter = false
					bool possible_g_isBetter = false;

					// If (nextNode is not in OpenList)
					if (!openList.Contains(nextNode))
					{
						// Add nextNode to OpenList
						openList.Add(nextNode);

						// nextNode.h = EstimateCost (nextNode, goalNode)
						nextNode->GetEstimateCost();

						// possible_g_isBetter = true
						possible_g_isBetter = true;
					}
					// Else If (possible_g < nextNode.g)
					else if (possible_g < nextNode->G)
					{
						// possible_g_isBetter = true
						possible_g_isBetter = true;
					}

					// If (possible_g_isBetter is true)
					if (possible_g_isBetter)
					{
						// nextNode.cameFrom = currentNode
						nextNode->Parent = currentNode;

						//nextNode.g = possible_g
						nextNode->G = possible_g;

						//nextNode.f = nextNode.g + nextNode.h
						nextNode->F = nextNode->G + nextNode->F;
					}
				}
			}

			// Check the buttom neighbour
			// Check to ensure not out of range
			if (currentNode->X - 1 > 0)
			{
				// Get the Left neighbor from the list
				nextNode = WorldArray[currentNode->X - 1][currentNode->Y];
				// Check to make sure the node hasn't been visited AND is not closed (Land)
				if (nextNode->GridType != GridNode::Land && closeList.Contains(nextNode))
				{
					// Skip nextNode
					continue;;
				}
				else
				{
					// possible_g = currentNode.g + DistanceBetween(currentNode, nextNode)
					float possible_g = currentNode->G + CalculateDistanceBetween(currentNode, nextNode);

					// possible_g_isBetter = false
					bool possible_g_isBetter = false;

					// If (nextNode is not in OpenList)
					if (!openList.Contains(nextNode))
					{
						// Add nextNode to OpenList
						openList.Add(nextNode);

						// nextNode.h = EstimateCost (nextNode, goalNode)
						nextNode->GetEstimateCost();

						// possible_g_isBetter = true
						possible_g_isBetter = true;
					}
					// Else If (possible_g < nextNode.g)
					else if (possible_g < nextNode->G)
					{
						// possible_g_isBetter = true
						possible_g_isBetter = true;
					}

					// If (possible_g_isBetter is true)
					if (possible_g_isBetter)
					{
						// nextNode.cameFrom = currentNode
						nextNode->Parent = currentNode;

						//nextNode.g = possible_g
						nextNode->G = possible_g;

						//nextNode.f = nextNode.g + nextNode.h
						nextNode->F = nextNode->G + nextNode->F;
					}
				}
			}
			*/
		}
		else
		{
			break;
		}
	}

	if (isGoalFound)
	{
		RenderPath();
		Ship->GeneratePath = false;
	}

	// clear openList & closeList
	for (GridNode* node : openList)
	{
		if (node)
		{
			node->Parent = nullptr;
		}
	}
	for (GridNode* node : closeList)
	{
		if (node)
		{
			node->Parent = nullptr;
		}
	}
}

TArray<GridNode*> ALevelGenerator::GetNeighbors(GridNode* current)
{
	TArray<GridNode*> neighborNodes;

	if (current->Y > 0)
	{
		// Left position node
		neighborNodes.Add(WorldArray[current->X][current->Y - 1]);
	}

	if (current->Y < MapSizeY - 1)
	{
		// Right position node
		neighborNodes.Add(WorldArray[current->X][current->Y + 1]);
	}

	if (current->X > 0)
	{
		// Bottom position node
		neighborNodes.Add(WorldArray[current->X - 1][current->Y]);
	}

	if (current->X < MapSizeX - 1)
	{
		// Top position node
		neighborNodes.Add(WorldArray[current->X + 1][current->Y]);
	}

	return neighborNodes;
}
void ALevelGenerator::RenderPath()
{
	UWorld* World = GetWorld();
	GridNode* CurrentNode = GoalNode;

	while (CurrentNode->Parent != nullptr)
	{
		FVector Position(CurrentNode->X * GRID_SIZE_WORLD, CurrentNode->Y * GRID_SIZE_WORLD, 20);
		// Spawn Path Actors
		AActor* PDActor = World->SpawnActor<AActor>(PathDisplayBlueprint, Position, FRotator::ZeroRotator);
		PathDisplayActors.Add(PDActor);

		Ship->Path.EmplaceAt(0, WorldArray[CurrentNode->X][CurrentNode->Y]);
		CurrentNode = CurrentNode->Parent;
	}
}

void ALevelGenerator::ResetPath()
{
	IsPathCalculated = false;
	SearchCount = 0;
	ResetAllNodes();

	for (int i = 0; i < PathDisplayActors.Num(); i++)
	{
		PathDisplayActors[i]->Destroy();
	}

	Ship->Path.Empty();
}

GridNode* ALevelGenerator::RemoveNodeWithSmallest_f(TArray<GridNode*>& openList)
{
	GridNode* retVal = nullptr;

	if (openList.Num() > 0 && openList[0])
	{
		//assume the lowest fitness node is the first one in the list
		int selectedIdx = 0;
		float lowestFitness = openList[0]->F;
		retVal = openList[0];

		//go through each node and see if there are any lower ones
		for (int i = 0; i < openList.Num(); ++i)
		{
			GridNode* gn = openList[i];

			if (gn)
			{
				//found a lower fitness!
				if (gn->F < lowestFitness)
				{
					lowestFitness = gn->F;
					selectedIdx = i;
				}
			}
		}

		if (selectedIdx != -1)
		{
			retVal = openList[selectedIdx];
			openList.RemoveAt(selectedIdx);
		}
	}

	return retVal;
}
